use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let mut children = vec![];

    for thread_num in 0..N {
        let tx_thread = mpsc::Sender::clone(&tx);
        children.push(thread::spawn(move || {
            tx_thread.send(format!("Output from thread {}", thread_num)).unwrap();
            thread::sleep(Duration::from_secs(1));
        }));
    }
    drop(tx);
    for child in children {
        child.join().unwrap();
    }
    for received in rx {
        println!("Got: {}", received);
    }
}
