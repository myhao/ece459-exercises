use std::thread;

static N: i32 = 20;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for thread_num in 0..N {
        children.push(thread::spawn(move || {
            println!("Output from thread {}", thread_num);
        }));
    }

    for child in children {
        child.join().unwrap();
    }
}
