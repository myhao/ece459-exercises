// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut result = 1;
    let mut prev = 0;
    for _ in 1..n {
        let tmp = result;
        result += prev;
        prev = tmp;
    }
    result
}


fn main() {
    for n in 1..11 {
        println!("{}", fibonacci_number(n));
    }
}
